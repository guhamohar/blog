json.array!(@posts) do |post|
  json.extract! post, :id, :title, :cd, :blog
  json.url post_url(post, format: :json)
end
